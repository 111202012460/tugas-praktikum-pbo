package week5;
import java.util.Arrays;
import java.util.Scanner;

class operasi {
	int[] arr = new int[5];
	Scanner input = new Scanner (System.in);
	
	public void SetArray() {//menampilkan urutan isi array
		for(int i=0 ; i<5 ;i++) {
			System.out.print("Masukkan Angka Ke-"+ (i+1) +": ");
			arr[i] = input.nextInt();
		}
	}
	
	public void cetakArray() {//menampilkan semua isi array
		System.out.println("isi Array :");
		for( int i = 0 ; i<5 ;i++ ) {
			System.out.print(arr[i]+",");
		}
	}
	
	public int sumOfArray() {//jumlah semua isi array
		int jumlah = 0;
		for( int i = 0 ; i<5 ;i++ ) {
			jumlah = jumlah + arr[i];
		}
		return jumlah;
	}
	
}

public class Array {

	public static void main(String[] args) {
		operasi A = new operasi();
		A.SetArray();
		A.cetakArray();
		System.out.println("Jumlah Angka = "+A.sumOfArray());
	}

}
