package week5;

import java.util.Scanner;
import java.util.ArrayList;

public class MahasiswaArrayList {
	static int n;
	public static void main(String[] args) {
		System.out.print("Masukkan Jumlah Data: ");
		try (Scanner inp = new Scanner(System.in)) {
			n = inp.nextInt();
			String[] nim = new String[n];
			String[] nama = new String[n];
			Double[] ipk = new Double[n];
			
			for (int i=0 ; i<n ; i++) {
				System.out.print("Masukkan NIM Anda: ");
				nim[i] = inp.next();
				System.out.print("Masukkan Nama Anda: ");
				nama[i] = inp.next();
				System.out.print("Masukkan IPK Anda: ");
				ipk[i] = inp.nextDouble();
				
			}
			setMahasiswa(nim, nama, ipk);
		}
		
	}
	
	public static void setMahasiswa(String[] nim, String[] nama, Double[] ipk) {
		ArrayList<Mahasiswa> a = new ArrayList<Mahasiswa>();
		for (int i = 0 ; i<n ;i++) {
			Mahasiswa mahasiswa = new Mahasiswa(nim[i], nama[i], ipk[i]);
			a.add(mahasiswa);
		}
		cetakMahasiswa(a);
	}
	
	public static void cetakMahasiswa(ArrayList<Mahasiswa> list) {
		for (int i = 0 ; i<n ;i++) {
			Mahasiswa data = list.get(i);
			System.out.println("Data ke-"+(i+1)+":");
			System.out.println("NIM :"+data.nim);
			System.out.println("Nama :"+data.nama);
			System.out.println("IPK :"+data.ipk);
		}
	}

}