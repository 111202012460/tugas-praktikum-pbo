package week5;

import java.util.Vector;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

class Kendaraan{
	String jenis;
	String warna;
	int plat;
	
	public Kendaraan(String jenis, String warna , int plat) {
		this.jenis = jenis;
		this.warna = warna;
		this.plat = plat;
	}
}

public class SomeVector {
	static int a;
	
	public static void main(String[] args)
	{
		Vector<String> mamalia = new Vector<>();
		Vector<String> unggas = new Vector<>();
		Vector<Integer> i = new Vector<>();
		Vector<Kendaraan> k = new Vector<>();
		
		mamalia.add("Kucing");//0
		mamalia.add("Sapi");//1
		mamalia.add("Kuda");//2
		unggas.add("Bebek");//0
		unggas.add("Ayam");//1
		unggas.add(1,"Elang");
		
		System.out.println(mamalia.get(2));
		System.out.println(unggas);
		
		Vector<String> hewan = new Vector<>();
		hewan.addAll(mamalia);
		hewan.add("Platipus");//0
		hewan.addAll(unggas);
		System.out.println(hewan);
		hewan.set(3, "Buaya");
		
		System.out.println(hewan);
		Iterator<String> iterate = hewan.iterator();
		System.out.println("Isi Vektor: ");
		while(iterate.hasNext()) {
			System.out.print(iterate.next());
			System.out.print(", ");
		}
		System.out.println("");
		for(int j = 0;j < hewan.size(); j++)
		{
			System.out.print(hewan.get(j)+", ");
		}
		
		String element = hewan.remove(2);
		System.out.println("");
		System.out.println("Elemen yang dihapus adalah "+element);
		System.out.println(hewan);
		hewan.clear();
		System.out.println(hewan);
		
		System.out.println("Operasi Input Dimulai.....");
		System.out.print("Berapakah Data Yang Ingin Diinputkkan ?");
		Scanner input = new Scanner(System.in);
		a = input.nextInt();
		for (int z = 0; a>z ; z++) {
			System.out.print("Masukkan Jenis Mobil :");
			String jenis = input.next();
			System.out.print("Masukkan Warna Mobil :");
			String warna = input.next();
			System.out.print("Masukkan Nomor Plat Mobil :");
			int plat = input.nextInt();
			Kendaraan kendaraan = new Kendaraan(jenis, warna, plat);
			k.add(kendaraan);
		}
		cetak(k);
		
	}
	public static void cetak(Vector<Kendaraan> list) {
		for (int i = 0 ; i<a ;i++) {
			Kendaraan data = list.get(i);
			System.out.println("Mobil ke-"+(i+1)+": ");
			System.out.println("Jenis : "+data.jenis);
			System.out.println("Warna : "+data.warna);
			System.out.println("Plat : "+data.plat);
		}
	}
}
